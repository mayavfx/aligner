"""Test the align module."""

# Import built-in modules
import unittest

# Import third-party modules
from maya import cmds  # pylint: disable=import-error

# Import local modules
from aligner import align
from aligner.align import Element


class TestAlign(unittest.TestCase):
    """Test the align module.

    In order to ensure that our align function does only alter one axis and
    does not affect the other axis, we check all coordinates of all objects.

    """

    def setUp(self):
        """Set up test and create some objects to test with."""
        self.handler = align.AlignHandler()

        self.test_objects = [
            cmds.polyCube(name="TestCube"),
            cmds.polyTorus(name="TestTorus"),
            cmds.polySphere(name="TestSphere"),
            cmds.polyCone(name="TestCone"),
        ]
        cmds.move(1, 2, 3, self.test_objects[0])
        cmds.move(2, 3, 4, self.test_objects[1])
        cmds.move(3, 4, 5, self.test_objects[2])
        cmds.move(4, 5, 6, self.test_objects[3])

        cmds.select(deselect=True)
        for element in self.test_objects:
            cmds.select(element[0], add=True)

    def tearDown(self):
        """Clean up after test and remove test objects."""
        for element in self.test_objects:
            cmds.delete(element)

    @staticmethod
    def _ensure_coordinates(object_, expected):
        """Get the x, y, z coordinates from the given object.

        This method rounds the coordinates to 3 digits which should be good
        enough for these tests to double check that we name the correct
        coordinate values. Using the ui, we should easily see that the other
        coordinate values won't be affected.

        Args:
            object_ (:obj:`list`): The Transform and shape of the object to
                get the coordinates from.
            expected (float, float, float): The x, y and z coordinates that
                are expected and will be used to match against.

        Returns:
            float, float, float: The x, y and z coordinates of the given
                object.

        """
        x_pos = round(cmds.getAttr(object_[0] + ".translateX"), 3)
        y_pos = round(cmds.getAttr(object_[0] + ".translateY"), 3)
        z_pos = round(cmds.getAttr(object_[0] + ".translateZ"), 3)

        return (x_pos, y_pos, z_pos) == expected

    def test_load(self):
        """Ensure we create Element instances for all selected objects."""
        expected = [
            Element(object='TestCube', x=1.0, y=2.0, z=3.0),
            Element(object='TestTorus', x=2.0, y=3.0, z=4.0),
            Element(object='TestSphere', x=3.0, y=4.0, z=5.0),
            Element(object='TestCone', x=4.0, y=5.0, z=6.0)
        ]

        calculated = self.handler.load()

        self.assertListEqual(expected, calculated)

    def test_align_top(self):
        """Ensure the objects get aligned and moved to top most object.

        This tests 'align_to_edge_object' by invoking it indirectly. As the
        top most object ist our 'TestCone' with y position in 5.0, all other
        object should have the same y position.

        """
        self.handler.align("y", "align_top")

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[0], (1.0, 5.0, 3.0)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[1], (2.0, 5.0, 4.0)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[2], (3.0, 5.0, 5.0)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[3], (4.0, 5.0, 6.0)))

    def test_align_vertical_median(self):
        """Ensure that all object's y position is in the median.

        This tests 'align_to_median' by invoking it indirectly. The y-positions
        of the objects are located in 2.0, 3.0, 4.0 and 5.0. The median of
        these values is 3.5. When aligning in y axis using a median, all
        object's y coordinates should have this value then.

        """
        self.handler.align("y", "align_horizontal")

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[0], (1.0, 3.5, 3.0)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[1], (2.0, 3.5, 4.0)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[2], (3.0, 3.5, 5.0)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[3], (4.0, 3.5, 6.0)))

    def test_distribute_horizontal(self):
        """Ensure that all object's are distributed in z axis as expected.

        This tests 'distribute_from_edge_object' by invoking it indirectly.
        In this test, we move the first test object to the z value of 2 and
        the last test object to the z axis value of 10. The test objects in
        between should be distributed evenly in the z-axis to 4.667 and 7.333.

        """
        cmds.setAttr(self.test_objects[0][0] + ".translateZ", 2)
        cmds.setAttr(self.test_objects[-1][0] + ".translateZ", 10)
        self.handler.align("z", "distribute_horizontal")

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[0], (1.0, 2.0, 2.0)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[1], (2.0, 3.0, 4.667)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[2], (3.0, 4.0, 7.333)))

        self.assertTrue(
            self._ensure_coordinates(self.test_objects[3], (4.0, 5.0, 10.0)))


def main():
    """Run unittests."""
    unittest.main()


main()
