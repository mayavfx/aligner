"""Utility functions."""

# Import built-in modules
import os
import webbrowser


def open_docs():
    """Open the documentation page."""
    url = ("http://www.leafpictures.de/projects/code/aligner/docs/html/"
           "index.html")
    webbrowser.open(url)


def get_version():
    """Get version of this package.

    Returns:
        str: Version of this package in the format major.minor.patch.

    """
    this_dir = os.path.dirname(__file__)
    package_root = os.path.join(this_dir, "..")
    package_root = os.path.abspath(package_root)
    package_name = os.path.basename(package_root)
    return package_name.split("_")[-1]
