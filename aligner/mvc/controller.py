"""Controller to drive the view."""

from aligner import align
from aligner.mvc import gui_helper


class Controller(object):
    """Drive the view instance."""

    def __init__(self, view):
        """Initialize the Controller instance.

        Args:
            view (aligner.mvc.view.View): View to drive.

        """
        self.view = view
        self.lock = None
        self.align_handler = align.AlignHandler()

        self.axis = None
        self.edge_object = None

        self.set_default_values()
        self.create_signals()

    def create_signals(self):
        """Create signals."""
        # We need anonymous functions in here, otherwise, the controller will
        # be referring to a different function object and will not execute
        # anything.
        # pylint: disable=unnecessary-lambda
        self.view.push_close.clicked.connect(self.view.close)
        self.view.key_pressed.connect(
            lambda key: self._handle_key_pressed(key))
        self.view.axis_grid.button_clicked.connect(
            lambda button: self.set_axis(button.objectName()))
        self.view.align_grid.button_clicked.connect(
            lambda button: self.process(button.objectName()))
        self.view.check_lock_x.clicked.connect(
            lambda: self._uncheck_other_locks(self.view.check_lock_x))
        self.view.check_lock_y.clicked.connect(
            lambda: self._uncheck_other_locks(self.view.check_lock_y))
        self.view.check_lock_z.clicked.connect(
            lambda: self._uncheck_other_locks(self.view.check_lock_z))

    def process(self, mode):
        """Process the align/ distribute action.

        Args:
            mode (str): Mode of alignment/ distribution to perform.

        """
        self.align_handler.align(self.axis, mode, lock=self.lock)

    def _uncheck_other_locks(self, lock):
        """Un-check all other locks when checking one lock.

        We want to lock only to one additional axis.

        """
        if not lock.isChecked():
            self.lock = None
            return

        self.lock = lock.text()

        locks = (
            self.view.check_lock_x,
            self.view.check_lock_y,
            self.view.check_lock_z,
        )
        for lock_ in locks:
            lock_.setChecked(lock == lock_)

    def set_default_values(self):
        """Set default values for view."""
        self.set_axis("x")

    def set_axis(self, axis):
        """Set axis member based on the clicked button.

        Args:
            axis (str): Name of the axis to set.

        """
        self.axis = axis
        self._set_axis_style()

    def _set_axis_style(self):
        """Set the style for the clicked axis and the other axis."""
        for button in self.view.axis_grid.buttons:
            button.setProperty("style", "arrow")
            if button.objectName() == self.axis:
                button.setProperty("style", "arrow_highlight")
        gui_helper.set_style_sheet(self.view.axis_grid)

    def _handle_key_pressed(self, key):
        """Handle key presses events.

        Args:
            key (str): Name of the key being pressed.

        """
        actions = {
            "esc": self.view.close
        }
        actions[key]()
