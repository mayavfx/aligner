"""Gui related helper functionality."""

import os

try:
    from PySide import QtCore
    from PySide import QtGui
    from PySide import QtGui as QtWidgets
except ImportError:
    from PySide2 import QtCore
    from PySide2 import QtGui
    from PySide2 import QtWidgets


def show_message(window, message):
    """Show message box with message as content.

    Args:
        window (QtWidgets.QWidget): Parent window in order to keep the message
            box on top of the parent window.
        message (str): Message to display in the window.

    """
    dialog = QtWidgets.QMessageBox()
    dialog.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
    dialog.information(window, 'information', message)


def set_style_sheet(widget, style="styles.qss"):
    """Apply css style sheet to given widget.

    Args:
        widget (Qtwidgets.QWidget): Widget to apply styles to.
        style (str): Name of styles file to apply.

    """
    this_dir = os.path.join(os.path.dirname(__file__))

    styles = os.path.join(this_dir, "..", "styles", style)
    styles = os.path.normpath(styles)

    if os.path.isfile(styles):
        with open(styles) as file_:
            widget.setStyleSheet(file_.read())


def set_icon(widget, path):
    """Update the icon of the given widget to the given path.

    Args:
        widget (QtWidgets.QWidget): Widget to update the icon on.
        path (str): Absolute path of icon to update to.

    """
    widget.setIcon(QtGui.QIcon(path))


def load_icons():
    """Load all icons from the icons folder.

    This scans the icons directory and creates an icon dictionary using the
    file names without extension as keys.

    Returns:
        dict: Dictionary of icons in the format:
            {
                "icon1": "path1",
                "icon2": "path2",
                "icon3": "path3",
                ...
            }

    """
    this_dir = os.path.dirname(__file__)
    dir_icon = os.path.join(this_dir, "..", "icons")
    dir_icon = os.path.normpath(dir_icon)

    icons = {}
    for file_ in os.listdir(dir_icon):
        name = os.path.splitext(file_)[0]
        path = os.path.join(dir_icon, file_)
        icons[name] = path

    return icons
