"""Main aligner window."""

# Import third-party modules
try:
    from PySide import QtCore
    from PySide import QtGui as QtWidgets
except ImportError:
    from PySide2 import QtCore
    from PySide2 import QtWidgets

# Import local modules
from aligner import utils
from aligner.mvc import gui_helper

# Icons for this package
ICONS = gui_helper.load_icons()


# We need multiple instance members here as we need to refer to them down the
# line. Additionally, in order to increase readability, we want to define the
# members in the location where they are used and not do a 'initialize
# everything in the __init__'.
# pylint: disable=too-many-instance-attributes, attribute-defined-outside-init
class View(QtWidgets.QDialog):
    """Main aligner view."""

    key_pressed = QtCore.Signal(str)

    def __init__(self, parent=None):
        """Initialize the View instance."""
        super(View, self).__init__(parent=parent)

        self.setup_ui()
        self.build_ui()
        self.create_tooltips()

        gui_helper.set_style_sheet(self)

    def setup_ui(self):
        """Setup ui."""
        self.setWindowTitle("Aligner {}".format(utils.get_version()))
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setFixedSize(QtCore.QSize(500, 500))

    def build_ui(self):
        """Build ui."""
        self.create_widgets()
        self.create_layouts()

    def create_widgets(self):
        """Create widgets."""
        self.axis_grid = AxisGrid()
        self.align_grid = AlignGrid()

        self.label_lock = QtWidgets.QLabel("lock")
        self.check_lock_x = QtWidgets.QCheckBox("x")
        self.check_lock_y = QtWidgets.QCheckBox("y")
        self.check_lock_z = QtWidgets.QCheckBox("z")
        self.push_close = QtWidgets.QPushButton("Close")

    def create_layouts(self):
        """Create layouts."""
        self.layout_main = QtWidgets.QVBoxLayout()
        self.layout_lock = QtWidgets.QHBoxLayout()

        self.layout_main.addWidget(self.axis_grid)
        self.layout_main.addWidget(self.align_grid)
        self.layout_main.addLayout(self.layout_lock)
        self.layout_main.addWidget(self.push_close)

        self.layout_lock.addStretch()
        self.layout_lock.addWidget(self.label_lock)
        self.layout_lock.addWidget(self.check_lock_x)
        self.layout_lock.addWidget(self.check_lock_y)
        self.layout_lock.addWidget(self.check_lock_z)
        self.layout_lock.addStretch()

        self.setLayout(self.layout_main)

    def create_tooltips(self):
        """Create tooltips."""
        self.check_lock_x.setToolTip("Lock elements sort also to X")
        self.check_lock_y.setToolTip("Lock elements sort also to Y")
        self.check_lock_z.setToolTip("Lock elements sort also to Z")

    def keyPressEvent(self, event):
        """Overwrite keyPressEvent to close window when hitting escape."""
        if event.key() == QtCore.Qt.Key_Escape:
            self.key_pressed.emit("esc")


class AlignGrid(QtWidgets.QWidget):
    """Grid with arrow buttons."""

    button_clicked = QtCore.Signal(QtWidgets.QWidget)

    def __init__(self):
        """Initialize the AlignGrid instance."""
        super(AlignGrid, self).__init__()

        self.active_button = None
        self.arrow_buttons = []

        self.build_ui()

    def build_ui(self):
        """Create widgets."""
        self.layout_main = QtWidgets.QGridLayout()
        image_names = (
            "align_top", "align_horizontal", "align_bottom",
            "align_left", "align_vertical", "align_right",
            "distribute_left", "distribute_horizontal", "distribute_right",
            "distribute_top", "distribute_vertical", "distribute_bottom"
        )

        row = 0
        column = 0
        for name in image_names:
            button = QtWidgets.QPushButton()
            button.setProperty("style", "arrow")
            button.setIconSize(QtCore.QSize(30, 30))
            button.setToolTip(name.replace("_", " "))
            button.setObjectName(name)
            gui_helper.set_icon(button, ICONS[name])

            if column > 2:
                column = 0
                row += 1

            self.layout_main.addWidget(button, row, column)

            button.clicked.connect(
                lambda button_=button: self.button_clicked.emit(button_))

            self.arrow_buttons.append(button)
            column += 1

        self.setLayout(self.layout_main)
        gui_helper.set_style_sheet(self)


class AxisGrid(QtWidgets.QWidget):
    """Grid with axis buttons."""

    button_clicked = QtCore.Signal(QtWidgets.QWidget)

    def __init__(self):
        """Initialize the AxisGrid instance."""
        super(AxisGrid, self).__init__()
        self.active_button = None
        self.buttons = []

        self.build_ui()

    def build_ui(self):
        """Create widgets."""
        self.layout_main = QtWidgets.QHBoxLayout()

        for axis in ("x", "y", "z"):
            button = QtWidgets.QPushButton(axis.upper())
            button.setObjectName(axis)

            self.layout_main.addWidget(button)

            button.clicked.connect(
                lambda button_=button: self.button_clicked.emit(button_))

            self.buttons.append(button)

        self.setLayout(self.layout_main)
        gui_helper.set_style_sheet(self)
