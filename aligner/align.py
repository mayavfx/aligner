"""Align- and distribution handler that positions all selected elements.

The AlignHandler creates internal Elements instances for all selected objects.
It will then sort the elements by their coordinates and perform the correct
calculations to align or evenly distribute the objects based on the axis,
mode and lock.

Usage:
    >>> from aligner import align

    # Let's create an align handler instance.
    >>> aligner = align.AlignHandler()

    # We need to select some objects now and can then align/ distribute these.
    # Let's align the objects in y-axis so that all selected objects will be
    # moved to the top most object.
    >>> aligner.align("y", "align_top")

    # Let's evenly distribute multiple objects along the x axis.
    >>> aligner.align("x", "distribute_horizontal")

"""

# Import built-in modules
from collections import namedtuple

# Import third-party modules
from maya import cmds  # pylint: disable=import-error

# Element tuple which holds the underlying object and its coordinates.
Element = namedtuple('Element', 'object x y z')

class AlignHandler(object):
    """Align- and distribution handler that positions all selected elements."""

    def __init__(self):
        """Initialize the AlignHandler instance."""
        self._operators = {
            "align_top": (-1, self._align_to_edge_object),
            "align_bottom": (0, self._align_to_edge_object),
            "align_left": (0, self._align_to_edge_object),
            "align_right": (-1, self._align_to_edge_object),
            "align_horizontal": (None, self._align_to_median),
            "align_vertical": (None, self._align_to_median),
            "distribute_top": (-1, self._distribute_from_edge_object),
            "distribute_bottom": (0, self._distribute_from_edge_object),
            "distribute_left": (-1, self._distribute_from_edge_object),
            "distribute_right": (0, self._distribute_from_edge_object),
            "distribute_horizontal": (0, self._distribute_from_edge_object),
            "distribute_vertical": (0, self._distribute_from_edge_object),
        }

    @staticmethod
    def load():
        """Load all selected objects as element instances.

        Returns:
            list: Elements instances of selected objects.

        """
        selection = cmds.ls(selection=True)
        elements = []
        for element in selection:
            x_pos = cmds.getAttr(element + ".translateX")
            y_pos = cmds.getAttr(element + ".translateY")
            z_pos = cmds.getAttr(element + ".translateZ")

            elements.append(Element(element, x_pos, y_pos, z_pos))

        return elements

    def align(self, axis, mode, lock=None):
        """Align or distribute the elements by given axis, mode and lock.

        Args:
            elements (:obj:`list` of :obj:`aligner.mvc.model.Element`):
                Selected elements as Element instances that include the
                underlying object and its coordinates.
            axis (str): Name of the axis to sort elements after.
            mode (str): Name of the alignment/ distribution mode to perform.
            lock (str | None): Optional axis to take into account when sorting.
                This is to prevent from mismatched element sorts where we might
                sometimes also take an additional axis into account.

        """
        elements = self.load()
        elements_sorted = self._sort_by_axis(elements, axis, lock)
        index = self._operators[mode][0]
        edge_element = None
        if index is not None:
            edge_element = elements_sorted[index]

        function = self._operators[mode][1]
        function(elements_sorted, edge_element, axis)

    @staticmethod
    def _sort_by_axis(elements, axis, lock):
        """Sort selection by given axis and optionally the lock.

        Args:
            axis (str): Name of the axis to sort elements after.
            elements (:obj:`list` of :obj:`aligner.mvc.model.Element`):
                Selected elements as Element instances that include the
                underlying object and its coordinates.
            lock (str | None): Optional axis to take into account when sorting.
                This is to prevent from mismatched element sorts where we might
                sometimes also take an additional axis into account.

        """
        if lock:
            sort = lambda element: (getattr(element, lock),
                                    getattr(element, axis))
        else:
            sort = lambda element: getattr(element, axis)

        return sorted(elements, key=sort)

    @staticmethod
    def _align_to_edge_object(elements, edge_element, axis):
        """Align all elements to the given edge_element.

        Args:
            elements (:obj:`list` of :obj:`aligner.mvc.model.Element`):
                Selected elements as Element instances that include the
                underlying object and its coordinates.
            edge_element (:obj:`aligner.mvc.model.Element`): Element to
                align to. This element won't be moved.
            axis (str): Name of the axis to sort elements after.

        """
        for element in elements:
            cmds.setAttr("{}.translate{}".format(
                element.object, axis.upper()), getattr(edge_element, axis))

    @staticmethod
    def _align_to_median(elements, _, axis):
        """Align the given elements into the median value of all elements.

        Args:
            elements (:obj:`list` of :obj:`aligner.mvc.model.Element`):
                Selected elements as Element instances that include the
                underlying object and its coordinates.
            axis (str): Name of the axis to sort elements after.

        """
        values = [getattr(element, axis) for element in elements]
        all_values = 0
        for value in values:
            all_values += value
        median = all_values / len(elements)

        for element in elements:
            cmds.setAttr("{}.translate{}".format(
                element.object, axis.upper()), median)

    @staticmethod
    def _distribute_from_edge_object(elements, edge_element_start, axis):
        """Create an even distribution between first and last element.

        Args:
            elements (:obj:`list` of :obj:`aligner.mvc.model.Element`):
                Selected elements as Element instances that include the
                underlying object and its coordinates.
            edge_element_start (:obj:`aligner.mvc.model.Element`): Element to
                align to. This element won't be moved.
            axis (str): Name of the axis to sort elements after.

        """
        edge_element_end = elements[0]
        if edge_element_start == elements[0]:
            edge_element_end = elements[-1]

        edge_position_1 = getattr(edge_element_start, axis)
        edge_position_2 = getattr(edge_element_end, axis)
        difference = abs(edge_position_1 - edge_position_2)
        factor = len(elements) - 1

        for index, element in enumerate(elements[1:-1]):
            value = (difference / factor) * (index + 1)

            if edge_position_1 > edge_position_2:
                new_position = getattr(edge_element_start, axis) - value
            else:
                new_position = getattr(edge_element_start, axis) + value

            position = "{}.translate{}".format(element.object, axis.upper())
            cmds.setAttr(position, new_position)
