"""Main entry point to create and set up the aligner window."""

# Import built-in modules
import sys

# Import third-party modules
try:
    from PySide import QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets

# Import local modules
from aligner.mvc import controller
from aligner.mvc import view

# Main window to show to the user.
_VIEW = None


def run():
    """Main entry point to create and set up the aligner window."""
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)

    # We need to make this global, otherwise, Maya will garbage collect the
    # view and will not show it.
    global _VIEW  # pylint: disable=global-statement
    _VIEW = view.View()
    _VIEW.raise_()
    _VIEW.show()

    controller.Controller(_VIEW)

    app.exec_()
