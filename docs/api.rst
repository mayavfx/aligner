.. _api:

API
---
The align package provides a public api that can be used to align and evenly
distribute selected objects in three dimensional space without using the
provided GUI. For that, you can use the ``AlignHandler`` object that is located
in the ``align`` module. The AlignHandler class is made public to the align
package itself and can be used like the following:

::

    >>> from aligner import AlignHandler

Let's now create an align handler instance::

    >>> aligner = AlignHandler()

We need to select some objects now and can then align/ distribute these. Let's
align the objects in y-axis so that all selected objects will be moved to the
top most object::

    >>> aligner.align("y", "align_top")

Let's evenly distribute multiple objects along the x axis::

    >>> aligner.align("x", "distribute_horizontal")
