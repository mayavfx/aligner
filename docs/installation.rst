.. _installation:

Installation
============
After having downloaded or cloned the aligner package, unpack it to any
location on your machine. The only thing you need to do now is adding the
aligner package root directory to the ``PYTHONPATH`` environment variable. You
can do this by updating your maya.env file using the following::

    PYTHONPATH=path/to/aligner_x.x.x

For more information about setting environment variables please have a look at
the official `documentation <https://autode.sk/2VfEyZI>`_.

Verify successful installation
------------------------------
When you have successfully installed the aligner package, you should have a
new menu in your menu bar called **Scripts**. In here you will find a sub menu
called **aligner**.

.. image:: img/menubar.jpg
