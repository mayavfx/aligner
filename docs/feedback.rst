.. _feedback:

Feedback
========
Any feedback about bug reports, feature requests, any additional ideas and
general feedback is highly welcome. Please send any of these to our support:
`info@leafpictures.de`_.

.. _info@leafpictures.de: info@leafpictures.de?subject=aligner

Credits
-------
**author:** Simon Jokuschies

**website:** www.cragl.com, www.leafpictures.de

**contact:** info@leafpictures.de
