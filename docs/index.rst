Welcome to aligner's documentation!
===================================
The aligner package provides a window to align and evenly distribute objects
in three dimensional space. Select some objects, set the axis to align or
distribute to and click the needed align or distribute button. Lock the
elements to an additional axis if required.

.. image:: img/aligner_maya.png

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   quickstart
   main_window
   api
   feedback
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
