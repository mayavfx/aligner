.. _main_window:

Main window
===========
The aligner main window contains the following sections:

.. image:: img/main_window.png

At the top you can select in which axis you want to align or evenly distribute
the objects to. You can align and evenly distribute to the X, Y and Z axis.

Below you find the following operations to choose from:

    - align Top
    - align horizontally
    - align bottom
    - align left
    - align vertical
    - align right
    - distribute left
    - distribute horizontal
    - distribute right
    - distribute top
    - distribute vertical
    - distribute_bottom

Locking elements to additional axis
-----------------------------------
Besides enabling one axis to process on, it is sometimes necessary to lock
to an additional axis. This can be useful, when you want to evenly distribute
elements and keep the element's axis hierarchy. Have a look at the following
distribute example.

In here, we distribute the elements along the y axis.

.. image:: img/lock_startup.jpg

However, when we do so, the Y positions are not continuously raising and we
get mismatches.

.. image:: img/lock_disabled.jpg

In that case, we can enable to lock additionally to the Z-Axis and this will
now correctly distribute the elements evenly with keeping the y positions
in tact.

.. image:: img/lock_enabled.jpg
