Changelog
=========

1.0.1
-----
  - Add credits section in documentation (#3)
  - Add docs/_build to gitignore (#4)

1.0.0
-----
  - Initial release (#1)
