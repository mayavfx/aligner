"""Maya internal userSetup to add the scripts menu to Maya's ui."""

# Import third-party modules
from pymel import core  # pylint: disable=import-error


def create_menu():
    """Create custom aligner menu in Scripts -> aligner."""
    scripts_menu = core.menu("scripts", parent="MayaWindow", label="Scripts")
    aligner_menu = core.menuItem(parent=scripts_menu, label="aligner",
                                 subMenu=True)

    core.menuItem(parent=aligner_menu, label="open",
                  command="from aligner import main; main.run()")

    core.menuItem(parent=aligner_menu, label="documentation",
                  command="from aligner import utils; utils.open_docs()")


core.evalDeferred("create_menu()")
