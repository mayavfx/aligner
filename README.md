Aligner
=======
version number: 1.0.1
author: Simon Jokuschies
website: www.cragl.com, www.leafpictures.de
contact: info@leafpictures.de

Overview
--------
The aligner package provides a window to align and evenly distribute objects
in three dimensional space. Select some objects, set the axis to align or
distribute to and click the needed align or distribute button. Lock the
elements to an additional axis if required.

Installation
------------
1) Add the aligner to your PYTHONPATH.
2) You will now have a new menu in your menubar: Scripts/aligner.
For more information please have a look at the documentation as well.

Feedback, bug reports
---------------------
Please send all requests to info@leafpictures.de and state your used operating
system and Maya version.
